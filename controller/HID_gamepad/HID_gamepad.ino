// Import libraries (BLEPeripheral depends on SPI)
#include <SPI.h>
#include <BLEHIDPeripheral.h>
#include <BLEGamepad.h>

// define pins (varies per shield/board)
#define BLE_REQ   -1
#define BLE_RDY   -1
#define BLE_RST   -1

// define pad gpios
#define DATA_CLOCK  2   //blue    2
#define DATA_LATCH  3   //yellow  3
#define SERIAL_DATA 4   //red     4

#define ANDROID_CENTRAL

// create peripheral instance, see pinouts above
BLEHIDPeripheral bleHIDPeripheral = BLEHIDPeripheral(BLE_REQ, BLE_RDY, BLE_RST);
BLEGamepad bleGamepad = BLEGamepad();
BLEBondStore bondStore = BLEBondStore();
  
void setup() {
  pinMode(DATA_CLOCK, OUTPUT);
  pinMode(DATA_LATCH, OUTPUT);
  pinMode(SERIAL_DATA, INPUT);
  Serial.begin(9600);

#if defined (__AVR_ATmega32U4__)
  while(!Serial);
#endif

  // clear bond store data
  bleHIDPeripheral.setBondStore(bondStore);
  bleHIDPeripheral.clearBondStoreData();

#ifdef ANDROID_CENTRAL  
  bleHIDPeripheral.setReportIdOffset(1);
#endif

  bleHIDPeripheral.setLocalName("Gamuino");
  bleHIDPeripheral.setAppearance(964);
  bleHIDPeripheral.setDeviceName("Gamuino");
  bleHIDPeripheral.addHID(bleGamepad);

  bleHIDPeripheral.begin();
}

gamepad_report_t readGamepadReport() {
  gamepad_report_t report = {};
  int arr[16];
  
  digitalWrite(DATA_LATCH, HIGH);
  delayMicroseconds(12);
  digitalWrite(DATA_LATCH, LOW);

  // 1. Cycle
  arr[0] = digitalRead(SERIAL_DATA);
  
  for (int i = 1; i < 16; i++) {
      digitalWrite(DATA_CLOCK, HIGH);
      delayMicroseconds(3);
      arr[i] = digitalRead(SERIAL_DATA);
      delayMicroseconds(3);
      digitalWrite(DATA_CLOCK, LOW);
      delayMicroseconds(6);    
  }

  report.b          = arr[0]  == 0 ? BUTTON_B      : 0;
  report.y          = arr[1]  == 0 ? BUTTON_Y      : 0;
  report.select     = arr[2]  == 0 ? BUTTON_SELECT : 0;
  report.start      = arr[3]  == 0 ? BUTTON_START  : 0;
  report.dpad_up    = arr[4]  == 0 ? DPAD_UP       : 0;
  report.dpad_down  = arr[5]  == 0 ? DPAD_DOWN     : 0;
  report.dpad_left  = arr[6]  == 0 ? DPAD_LEFT     : 0;
  report.dpad_right = arr[7]  == 0 ? DPAD_RIGHT    : 0;
  report.a          = arr[8]  == 0 ? BUTTON_A      : 0;
  report.x          = arr[9]  == 0 ? BUTTON_X      : 0;
  report.l          = arr[10] == 0 ? BUTTON_L      : 0;
  report.r          = arr[11] == 0 ? BUTTON_R      : 0;

  return report;
}

gamepad_report_t last_report = {}; 
byte incomingByte = 0;
void loop() {
  BLECentral central = bleHIDPeripheral.central();
  if (central) {
    // central connected to peripheral
    bool is_connected = central.connected();
    while (is_connected) {
      gamepad_report_t new_report = readGamepadReport();
      if (new_report != last_report) {
        bleGamepad.press(new_report);
        last_report = new_report; 
      }
  
      delay(20);
      is_connected = central.connected();
    }

    if (!is_connected) {
      bondStore.clearData();      
    }
  }
}
