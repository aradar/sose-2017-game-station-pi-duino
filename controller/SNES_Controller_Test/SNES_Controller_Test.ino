#define DATA_CLOCK  2   //Blau 2
#define DATA_LATCH  3   //Gelb 3
#define SERIAL_DATA 4   //Rot  4

unsigned long time;

void setup()
{
  pinMode(DATA_CLOCK, OUTPUT);
  pinMode(DATA_LATCH, OUTPUT);
  pinMode(SERIAL_DATA, INPUT);

  Serial.begin(9600);
}

void loop()
{ 
  int arr[16];

  digitalWrite(DATA_LATCH, HIGH);
  delayMicroseconds(12);
  digitalWrite(DATA_LATCH, LOW);

  // 1. Cycle
  //delayMicroseconds(6);
  arr[0] = digitalRead(SERIAL_DATA);
  //delayMicroseconds(6);  
  
  for (int i = 1; i < 16; i++) {
      digitalWrite(DATA_CLOCK, HIGH);
      delayMicroseconds(3);
      arr[i] = digitalRead(SERIAL_DATA);
      delayMicroseconds(3);
      digitalWrite(DATA_CLOCK, LOW);
      delayMicroseconds(6);    
  }
  
  Serial.print("B: ");
  Serial.println(arr[0]);
  Serial.print("Y: ");
  Serial.println(arr[1]);
  Serial.print("SELECT: ");
  Serial.println(arr[2]);
  Serial.print("START: ");
  Serial.println(arr[3]);
  Serial.print("UP: ");
  Serial.println(arr[4]);
  Serial.print("DOWN: ");
  Serial.println(arr[5]);
  Serial.print("LEFT: ");
  Serial.println(arr[6]);
  Serial.print("RIGHT: ");
  Serial.println(arr[7]);
  Serial.print("A: ");
  Serial.println(arr[8]);
  Serial.print("X: ");
  Serial.println(arr[9]);
  Serial.print("L: ");
  Serial.println(arr[10]);
  Serial.print("R: ");
  Serial.println(arr[11]);
  Serial.print("NONE: ");
  Serial.println(arr[12]);
  Serial.print("NONE: ");
  Serial.println(arr[13]);
  Serial.print("NONE: ");
  Serial.println(arr[14]);
  Serial.print("NONE: ");
  Serial.println(arr[15]);
  Serial.println("-----------------------");

  delay(1000);
}
